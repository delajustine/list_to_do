<?php header("Content-Type: text/html; charset=ISO-8859-1", true);

$codigo = $_GET['cod']; 

require_once('MVC/dao/TarefaDAO.php');

try{

    $daoTask = new TarefaDAO(null);
    $rsTask = $daoTask->getListaProjeto($codigo);

}
catch(Exception $e){
}

?>

<div class="table-responsive" >
                                                   
    <table class="table table-striped">
        
        <thead>
            <tr>
                <th>Tarefa</th>
                <th>Prioridade</th>
                <th>Status</th>
                <th>Entrega</th>
                <th>Tempo Estimado</th>
                <th width="100" colspan="2">A��es</th>
            </tr>
        </thead>
        
        <tbody>

            <?php while($rowTask = $rsTask->fetch(PDO::FETCH_ASSOC)) { ?>
                
                <tr id="linha-<?php echo $rowTask['codigo'];?>">
                   
                    <td><?php echo $rowTask['tarefa'];?></td>
                    <td>
                        <?php echo utf8_decode($daoTask->getPrioridade($rowTask['prioridade']));?></td>
                    <td>
                        <?php echo utf8_decode($daoTask->getStatus($rowTask['status']));?>
                    </td>
                    <td><?php echo $rowTask['entrega'];?></td>
                    <td><?php echo $rowTask['tempo'];?> hora(s)</td>

                    <td width="50">
                        <button type="button" class="btn btn-primary btn-xs btn-edit-task" id="<?php echo $rowTask['codigo'];?>">Editar</button>
                    </td>
                    <td width="50">
                        <button type="button" class="btn btn-danger btn-xs btn-delete-task" id="<?php echo $rowTask['codigo'];?>">Excluir</button>
                    </td>

                </tr>

            <?php }?>

        </tbody>

        
    </table>

</div><!-- END table-responsive -->

<div class="btn-add-task" id="<?php echo $codigo;?>">
    <button type="button" class="btn btn-warning btn-md">Adicionar Tarefa</button>
</div>