<?php
global $pasta;
$pasta = "/list-to-do/";
//$pasta = "/";

ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

// error_reporting(0);
// ini_set(“display_errors”, 0 );

class Conexao {

	public static $instance;

	private function __construct() {  }

	public static function getInstance() {

		if (!isset(self::$instance)) {
			
			self::$instance = new PDO('mysql:host=localhost;dbname=list_to_do', 'root', '');
			//self::$instance->exec("set names iso-8859-1");
			self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			self::$instance->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
		}

		return self::$instance;
	}
}
?>