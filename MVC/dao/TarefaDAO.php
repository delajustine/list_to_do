<?php
require_once('conexao.php');

require_once($_SERVER['DOCUMENT_ROOT'] . $pasta . 'MVC/model/Uteis.php');
require_once($_SERVER['DOCUMENT_ROOT'] . $pasta . 'MVC/model/Tarefa.php');

class TarefaDAO {

	private $sql;
	private $campos = array('projeto','tarefa','prioridade','status','entrega','tempo');/* Adicionar todos campos para insert, update */

	public $colunas;
	public $values;
	public $parameters = array();

	public $colunas_update;

    public function __construct(){

	foreach ($this->campos as $campo) {

		$this->colunas .=  $campo . ", ";
		$this->values .= ":" . $campo . ", ";

		$this->colunas_update .= $campo . " = :" . $campo . ", ";

	}

	$this->colunas = substr($this->colunas,0,-2);
	$this->values = substr($this->values,0,-2);
	$this->colunas_update = substr($this->colunas_update,0,-2);

    }

	public function Insert(Tarefa $obj){

		foreach ($this->campos as $campo) {
			$this->parameters[$campo] = $obj->$campo;
		}

		try {

			$this->sql = "INSERT INTO tarefa (" . $this->colunas . ") VALUES(" . $this->values . ") ";

			$p_sql = Conexao::getInstance()->prepare($this->sql);
			$p_sql->execute($this->parameters);

			return Conexao::getInstance()->lastInsertId();

		}catch(Exception $e){
			return $e->getMessage();
		}
	}

	

    public function Update(Tarefa $obj){

		foreach ($this->campos as $campo) {
			$this->parameters[':'.$campo] = $obj->$campo;
		}
		$this->parameters[':codigo'] = $obj->codigo;

		try {

        $this->sql = "UPDATE tarefa set " . $this->colunas_update . " WHERE codigo = :codigo ";

		$p_sql = Conexao::getInstance()->prepare($this->sql);
		$p_sql->execute($this->parameters);

        }catch(Exception $e){
			return $e->getMessage();
		}
		
    }

    public function Delete($codigo){

		try {

			$this->sql = "DELETE FROM tarefa WHERE codigo = :codigo";

			$p_sql = Conexao::getInstance()->prepare($this->sql);
			$p_sql->execute(array(':codigo' => $codigo));

        }catch(Exception $e){
			return $e->getMessage();
		}
    }    

    public function getById($codigo){
        $imo = null;

		try {

			$this->sql = "SELECT * FROM tarefa
						  WHERE codigo = :codigo ";

			$p_sql = Conexao::getInstance()->prepare($this->sql);
			$p_sql->execute(array(':codigo' => $codigo));

			if ($row = $p_sql->fetch(PDO::FETCH_ASSOC)){
				$obj = new Tarefa();
				$obj->codigo = $codigo;
				
				
				foreach ($this->campos as $campo) {
					$obj->$campo = $row[$campo];
				}
			}

		}catch(Exception $e){
			return $e->getMessage();
		}

        return ($obj);
    }

    public function getLista($inicio,$qnt){
		try {

			$this->sql = "SELECT * FROM tarefa  
						  ORDER BY codigo DESC LIMIT $inicio, $qnt ";	

			$rs = Conexao::getInstance()->prepare($this->sql);
			$rs->execute();

			return ($rs);

        }catch(Exception $e){
			return $e->getMessage();
		}
    }

    public function getListaProjeto($projeto){
		try {

			$this->sql = "SELECT * FROM tarefa  
						  WHERE projeto = :projeto
						  ORDER BY codigo ASC ";	

			$rs = Conexao::getInstance()->prepare($this->sql);
			$rs->execute(array(':projeto' => $projeto));

			return ($rs);

        }catch(Exception $e){
			return $e->getMessage();
		}
    }

    public function getPrioridade($prioridade){
    	if($prioridade == '1'){
    		return ("<div class='prioridade-alta'>Alta</div>");
    	}

    	if($prioridade == '2'){
    		return ("<div class='prioridade-media'>Média</div>");
    	}

    	if($prioridade == '3'){
    		return ("<div class='prioridade-pequena'>Pequena</div>");
    	}
    }

    public function getStatus($status){

        if($status == '1'){
    		return ("<div class='status-finalizada'>Finalizada</div>");
    	}

    	if($status == '2'){
    		return ("<div class='status-em-desenvolvimento'>Em desenvolvimento</div>");
    	}

    	if($status == '3'){
    		return ("<div class='status-em-revisao'>Em revisão</div>");
    	}

    	if($status == '4'){
    		return ("<div class='status-atrasada'>Atrasada</div>");
    	}

    }

}
?>
