<?php
require_once('conexao.php');

require_once($_SERVER['DOCUMENT_ROOT'] . $pasta . 'MVC/model/Uteis.php');
require_once($_SERVER['DOCUMENT_ROOT'] . $pasta . 'MVC/model/Projeto.php');

class ProjetoDAO {

	private $sql;
	private $campos = array('nome');/* Adicionar todos campos para insert, update */

	public $colunas;
	public $values;
	public $parameters = array();

	public $colunas_update;

    public function __construct(){

	foreach ($this->campos as $campo) {

		$this->colunas .=  $campo . ", ";
		$this->values .= ":" . $campo . ", ";

		$this->colunas_update .= $campo . " = :" . $campo . ", ";

	}

	$this->colunas = substr($this->colunas,0,-2);
	$this->values = substr($this->values,0,-2);
	$this->colunas_update = substr($this->colunas_update,0,-2);

    }

	public function Insert(Projeto $obj){

		foreach ($this->campos as $campo) {
			$this->parameters[$campo] = $obj->$campo;
		}

		try {

			$this->sql = "INSERT INTO projeto (" . $this->colunas . ") VALUES(" . $this->values . ") ";

			$p_sql = Conexao::getInstance()->prepare($this->sql);
			$p_sql->execute($this->parameters);

			return Conexao::getInstance()->lastInsertId();

		}catch(Exception $e){
			return $e->getMessage();
		}
	}

	

    public function Update(Projeto $obj){

		foreach ($this->campos as $campo) {
			$this->parameters[':'.$campo] = $obj->$campo;
		}
		$this->parameters[':codigo'] = $obj->codigo;

		try {

        $this->sql = "UPDATE projeto set " . $this->colunas_update . " WHERE codigo = :codigo ";

		$p_sql = Conexao::getInstance()->prepare($this->sql);
		$p_sql->execute($this->parameters);

        }catch(Exception $e){
			return $e->getMessage();
		}
		
    }

    public function Delete($codigo){

		try {

			$this->sql = "DELETE FROM projeto WHERE codigo = :codigo";

			$p_sql = Conexao::getInstance()->prepare($this->sql);
			$p_sql->execute(array(':codigo' => $codigo));

        }catch(Exception $e){
			return $e->getMessage();
		}
    }    

    public function DeleteTarefa($codigo){
    	try {

			$this->sql = "DELETE FROM tarefa WHERE projeto = :codigo";

			$p_sql = Conexao::getInstance()->prepare($this->sql);
			$p_sql->execute(array(':codigo' => $codigo));

        }catch(Exception $e){
			return $e->getMessage();
		}
    }

    public function getById($codigo){
        $imo = null;

		try {

			$this->sql = "SELECT * FROM projeto
						  WHERE codigo = :codigo ";

			$p_sql = Conexao::getInstance()->prepare($this->sql);
			$p_sql->execute(array(':codigo' => $codigo));

			if ($row = $p_sql->fetch(PDO::FETCH_ASSOC)){
				$obj = new Projeto();
				$obj->codigo = $codigo;
				
				
				foreach ($this->campos as $campo) {
					$obj->$campo = $row[$campo];
				}
			}

		}catch(Exception $e){
			return $e->getMessage();
		}

        return ($obj);
    }

    public function getLista($inicio,$qnt){
		try {

			$this->sql = "SELECT * FROM projeto  
						  ORDER BY codigo DESC LIMIT $inicio, $qnt ";	

			$rs = Conexao::getInstance()->prepare($this->sql);
			$rs->execute();

			return ($rs);

        }catch(Exception $e){
			return $e->getMessage();
		}
    }

}
?>
