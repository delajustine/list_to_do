<?php

require_once('Uteis.php');

class Tarefa {

    public $codigo;
    public $projeto;
    public $tarefa;
    public $prioridade;
    public $status;
    public $entrega;
    public $tempo;

    public function __construct() {
        $this->codigo = "";
		$this->projeto = "";
        $this->tarefa = "";
        $this->prioridade = "";
        $this->status = "";
        $this->entrega = "";
        $this->tempo = "";
    }

    public function ValidaCampos(){
		if (trim($this->projeto) == '')
			return(false);

        if (trim($this->tarefa) == '')
            return(false);

        if (trim($this->prioridade) == '')
            return(false);

        if (trim($this->status) == '')
            return(false);

        if (trim($this->entrega) == '')
            return(false);

        if (trim($this->tempo) == '')
            return(false);
        
        return(true);
    }

}
?>
