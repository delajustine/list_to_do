<?php

function AddMask($pMask, $pValue, $pReturnValueEmpty){
    if ($pReturnValueEmpty == true && ($pValue == null || trim($pValue) == ''))
    return '';

    $aux = '';
    $pos = 0;
    for($i = 0; $i <= strlen($pMask); $i++){
        if ($pMask[$i] == '#'){
            $aux = $aux . $pValue[$pos];
            $pos++;
        }
        else
        $aux = $aux . $pMask[$i];
    }
    return ($aux);
}

function RemoveMask($valor){
    $valor = str_replace('.','',$valor);
    $valor = str_replace('/','',$valor);
    $valor = str_replace('-','',$valor);
    $valor = trim($valor);
    return($valor);
}

function ValidarStr($texto){
    $flag = true;
    $tam = strlen($texto);
    $i = 0;
    while ($i<$tam && $flag){
        $ch = ord($texto[$i]);
        if (($ch == 34)||($ch == 39)||($ch == 59)||($ch == 60)){
            $flag = false;
        }
        $i++;
    }
    return ($flag);
}

function StrToFloat($valor){
    $aux = str_replace(',', '.', $valor);

    return($aux);
}

function FloatToStr($valor){
    $aux = str_replace('.', ',', $valor);

    return($aux);
}

function FormatarValor($valor, $tamanho){
    $aux = str_replace(',', '', $valor);
    $aux = str_replace('.', '', $aux);
    $aux = str_pad($aux, $tamanho, '0', STR_PAD_LEFT);

    return($aux);
}

function StrToSqlDate($data){
    $aux = substr($data,6,4).'-'.substr($data,3,2).'-'.substr($data,0,2);
    return($aux);
}

function SqlDateToStr($data){
    if ($data != '')
        $aux = substr($data,8,2).'/'.substr($data,5,2).'/'.substr($data,0,4);
    else
        $aux = '';
    return($aux);
}


function RemoveAcentos($str, $enc = 'UTF-8'){

    $acentos = array(
    'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
    'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
    'C' => '/&Ccedil;/',
    'c' => '/&ccedil;/',
    'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
    'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
    'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
    'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
    'N' => '/&Ntilde;/',
    'n' => '/&ntilde;/',
    'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
    'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
    'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
    'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
    'Y' => '/&Yacute;/',
    'y' => '/&yacute;|&yuml;/',
    'a.' => '/&ordf;/',
    'o.' => '/&ordm;/'
    );
    return preg_replace($acentos, array_keys($acentos), htmlentities($str,ENT_NOQUOTES, $enc));
}

function UpperAcentos($Msg){
    $a = array(
        '/�/'=>'�',
        '/�/'=>'�',
        '/�/'=>'�',
        '/�/'=>'�',
        '/�/'=>'�');

    return preg_replace(array_keys($a), array_values($a), $Msg);
 }

 function UltimoDia(){
    $year = date("Y");
    $month = date("m");
    return (date("t/m/Y", mktime(0, 0, 0, $month, 1, $year)));
 }

 function getDataExtenso($data){
     $mes = substr($data,3,2);
     if ($mes == "01")
        $aux = "Janeiro";
     else
     if ($mes == "02")
        $aux = "Fevereiro";
     else
     if ($mes == "03")
        $aux = "Mar�o";
     else
     if ($mes == "04")
        $aux = "Abril";
     else
     if ($mes == "05")
        $aux = "Maio";
     else
     if ($mes == "06")
        $aux = "Junho";
     else
     if ($mes == "07")
        $aux = "Julho";
     else
     if ($mes == "08")
        $aux = "Agosto";
     else
     if ($mes == "09")
        $aux = "Setembro";
     else
     if ($mes == "10")
        $aux = "Outubro";
     else
     if ($mes == "11")
        $aux = "Novembro";
     else
        $aux = "Dezembro";

     $aux = substr($data,0,2)." de ".$aux." de ".substr($data,6);
     return($aux);
 }

 function getDataExtenso3($data){
     $mes = substr($data,3,2);
     if ($mes == "01")
        $aux = "Janeiro";
     else
     if ($mes == "02")
        $aux = "Fevereiro";
     else
     if ($mes == "03")
        $aux = "Mar�o";
     else
     if ($mes == "04")
        $aux = "Abril";
     else
     if ($mes == "05")
        $aux = "Maio";
     else
     if ($mes == "06")
        $aux = "Junho";
     else
     if ($mes == "07")
        $aux = "Julho";
     else
     if ($mes == "08")
        $aux = "Agosto";
     else
     if ($mes == "09")
        $aux = "Setembro";
     else
     if ($mes == "10")
        $aux = "Outubro";
     else
     if ($mes == "11")
        $aux = "Novembro";
     else
        $aux = "Dezembro";

     $aux = substr($data,0,2)." de ".$aux;
     return($aux);
 }

 function getHoraExtenso($hora){
     $aux = substr($hora,0,2).'h '.substr($hora,3,2).'min';
     return($aux);
 }

 function getDataExtenso2($data){
     $mes = substr($data,3,2);
     if ($mes == "01")
        $aux = "JAN";
     else
     if ($mes == "02")
        $aux = "FEV";
     else
     if ($mes == "03")
        $aux = "MAR";
     else
     if ($mes == "04")
        $aux = "ABR";
     else
     if ($mes == "05")
        $aux = "MAI";
     else
     if ($mes == "06")
        $aux = "JUN";
     else
     if ($mes == "07")
        $aux = "JUL";
     else
     if ($mes == "08")
        $aux = "AGO";
     else
     if ($mes == "09")
        $aux = "SET";
     else
     if ($mes == "10")
        $aux = "OUT";
     else
     if ($mes == "11")
        $aux = "NOV";
     else
        $aux = "DEZ";

     return($aux);
 }


function getDataBr($data){
	  $rData = explode("-", $data);
	  $rData = $rData[2].'/'.$rData[1].'/'.$rData[0];
	  echo $rData;
}

function converte_string($text, $debug_on){

	$acentuadas     = array('�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�');
	$convertidas   = array('&aacute;', '&Aacute;', '&atilde;', '&Atilde;', '&acirc;', '&Acirc;', '&agrave;', '&Agrave;', '&eacute;', '&Eacute;', '&ecirc;', '&Ecirc;',  '&iacute;', '&Iacute', '&oacute;', '&Oacute;', '&otilde;', '&Otilde;', '&ocirc;', '&Ocirc;', '&uacute;', '&Uacute;', '&uuml;', '&Uuml;', '&ccedil;', '&Ccedil;');
	$saida  = str_replace($acentuadas, $convertidas, $text);

	if ($debug_on){
			echo $saida;
			var_dump($saida);
	}

	return $saida;
}

function GeraSenha(){
	$caracteres = 'abcdxywzABCDZYWZ0123456789';
	$max = strlen($caracteres)-1;
	$password = null;

	for($i=0; $i < 8; $i++){
		$password .= $caracteres{mt_rand(0, $max)};
	}
	return $password;
}

function extenso($valor = 0, $maiusculas = false) {
	$singular = array("centavo", "real", "mil", "milh�o", "bilh�o", "trilh�o", "quatrilh�o");
	$plural = array("centavos", "reais", "mil", "milh�es", "bilh�es", "trilh�es",
	"quatrilh�es");

	$c = array("", "cem", "duzentos", "trezentos", "quatrocentos",
	"quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
	$d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta",
	"sessenta", "setenta", "oitenta", "noventa");
	$d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze",
	"dezesseis", "dezesete", "dezoito", "dezenove");
	$u = array("", "um", "dois", "tr�s", "quatro", "cinco", "seis",
	"sete", "oito", "nove");

	$z = 0;
	$rt = "";

	$valor = number_format($valor, 2, ".", ".");
	$inteiro = explode(".", $valor);
	for($i=0;$i<count($inteiro);$i++)
		for($ii=strlen($inteiro[$i]);$ii<3;$ii++)
			$inteiro[$i] = "0".$inteiro[$i];

		$fim = count($inteiro) - ($inteiro[count($inteiro)-1] > 0 ? 1 : 2);
		for ($i=0;$i<count($inteiro);$i++) {
		$valor = $inteiro[$i];
		$rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
		$rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
		$ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

		$r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd &&
		$ru) ? " e " : "").$ru;
		$t = count($inteiro)-1-$i;
		$r .= $r ? " ".($valor > 1 ? $plural[$t] : $singular[$t]) : "";
		if ($valor == "000")$z++; elseif ($z > 0) $z--;
		if (($t==1) && ($z>0) && ($inteiro[0] > 0)) $r .= (($z>1) ? " de " : "").$plural[$t];
		if ($r) $rt = $rt . ((($i > 0) && ($i <= $fim) &&
			($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
		}

		if(!$maiusculas){
			return($rt ? $rt : "zero");
		} else {

		if ($rt) $rt=ereg_replace(" E "," e ",ucwords($rt));
		return (($rt) ? ($rt) : "Zero");
		}

}

function antiSqlInj($sql){
	// remove palavras que contenham sintaxe sql
	$sql = preg_replace(sql_regcase("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/"),"",$sql);
	$sql = trim($sql);//limpa espa�os vazio
	$sql = strip_tags($sql);//tira tags html e php
	$sql = addslashes($sql);//Adiciona barras invertidas a uma string
	return $sql;
}

function diasemana($data) {
	$ano =  substr("$data", 6, 9);
	$mes =  substr("$data", 3, -5);
	$dia =  substr("$data", 0, 2);

	$diasemana = date("w", mktime(0,0,0,$mes,$dia,$ano) );

	switch($diasemana) {
		case"0": $diasemana = "Domingo";       break;
		case"1": $diasemana = "Segunda-Feira"; break;
		case"2": $diasemana = "Ter�a-Feira";   break;
		case"3": $diasemana = "Quarta-Feira";  break;
		case"4": $diasemana = "Quinta-Feira";  break;
		case"5": $diasemana = "Sexta-Feira";   break;
		case"6": $diasemana = "S�bado";        break;
	}

	return $diasemana;
}

// Fun��o para limitar por palavra
function limit_words($string, $word_limit){
    $words = explode(" ",$string);
    return implode(" ",array_splice($words,0,$word_limit));
}

// Fun��o para deixar apenas a primeira letra de cada palavra maiscula
function primeiraMaiscula($string){
	$string = strtolower($string);
	$string = mb_strtolower($string, "iso-8859-1");
	$string = ucwords($string);
	return $string;
}

function getIdade($data){

	// Separa em dia, m�s e ano
	list($ano, $mes, $dia) = explode('-', $data);

	// Descobre que dia � hoje e retorna a unix timestamp
	$hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
	// Descobre a unix timestamp da data de nascimento do fulano
	$nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);

	// Depois apenas fazemos o c�lculo j� citado :)
	$idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

	return $idade;

}

function validaEmail($mail){
	if(preg_match("/^([[:alnum:]_.-]){3,}@([[:lower:][:digit:]_.-]{3,})(.[[:lower:]]{2,3})(.[[:lower:]]{2})?$/", $mail)) {
		return true;
	}else{
		return false;
	}
}

function getNomeMes($mes){

	switch ($mes) {
		case 1:
			return "Janeiro";
			break;
		case 2:
			return "Fevereiro";
			break;
		case 3:
			return "Mar�o";
			break;
		case 4:
			return "Abril";
			break;
		case 5:
			return "Maio";
			break;
		case 6:
			return "Junho";
			break;
		case 7:
			return "Julho";
			break;
		case 8:
			return "Agosto";
			break;
		case 9:
			return "Setembro";
			break;
		case 10:
			return "Outubro";
			break;
		case 11:
			return "Novembro";
			break;
		case 12:
			return "Dezembro";
			break;
	}

}

function getStatusPagamento($status){

	switch ($status) {
		case 1:
			return "Aguardando pagamento";
			break;
		case 2:
			return "Em an�lise";
			break;
		case 3:
			return "Paga";
			break;
		case 4:
			return "Dispon�vel";
			break;
		case 5:
			return "Em disputa";
			break;
		case 6:
			return "Devolvida";
			break;
		case 7:
			return "Cancelada";
			break;
	}

}

function getValorDesconto($desconto,$valor){

	$valor_desconto = ($valor * $desconto ) / 100;

	return $valor_desconto;
}

function clearTagBr($texto_br){

    $breaks = array("<br />","<br>","<br/>");
    $texto = str_ireplace($breaks, "\r\n", $texto_br);

    return $texto;
}


 

?>
