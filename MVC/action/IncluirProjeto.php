<?php header("Content-Type: text/html; charset=ISO-8859-1", true);

require_once ('../model/Projeto.php');
require_once ('../dao/ProjetoDAO.php');

$status = 'ok';
$dao = null;
try{

    $obj = new Projeto();
    $obj->nome = utf8_decode($_POST["Enome"]);

    if ($obj->ValidaCampos()){
        $dao = new ProjetoDAO(null);
        $codigo = $dao->Insert($obj);
    }
    else
        $status = 'aviso';
}
catch (Exception $e){
    $status = 'erro';
}

print($status);
?>

