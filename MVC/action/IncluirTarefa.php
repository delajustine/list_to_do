<?php header("Content-Type: text/html; charset=ISO-8859-1", true);

require_once ('../model/Tarefa.php');
require_once ('../dao/TarefaDAO.php');

$status = 'ok';
$dao = null;
try{

    $obj = new Tarefa();
    $obj->projeto = utf8_decode($_POST["EcodProject"]);
    $obj->tarefa = utf8_decode($_POST["Etarefa"]);
    $obj->prioridade = utf8_decode($_POST["Eprioridiade"]);
    $obj->status = utf8_decode($_POST["Estatus"]);
    $obj->entrega = utf8_decode($_POST["EdataEntrega"]);
    $obj->tempo = utf8_decode($_POST["Etempo"]);


    if ($obj->ValidaCampos()){
        $dao = new TarefaDAO(null);
        $codigo = $dao->Insert($obj);
    }
    else
        $status = 'aviso';
}
catch (Exception $e){
    $status = 'erro';
}

print($status);
?>

