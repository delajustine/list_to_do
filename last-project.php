<?php header("Content-Type: text/html; charset=ISO-8859-1", true);

require_once('MVC/dao/ProjetoDAO.php');

try{
    $dao = new ProjetoDAO(null);
    
    $rs = $dao->getLista(0,1);
    $numRows = $rs->rowCount();
    $row = $rs->fetch(PDO::FETCH_ASSOC);

}
catch(Exception $e){
}

?>
<div class="panel panel-primary" id="painel-<?php echo $row['codigo'];?>">

    <div class="panel-heading">
                                    
        <div class="pull-left title-list-project">
            <?php echo $row['nome'];?>
        </div>

        <div class="pull-right btn-delete-project" id="<?php echo $row['codigo'];?>">
            
        </div>

        <div class="clearfix"></div>

    </div>

    <div class="panel-body">
                                    
        <div id="list-task-<?php echo $row['codigo'];?>">
            
            <div class="add-first-task">
                Adicione a primeira tarefa no projeto.
            </div>

             <div class="btn-add-task" id="<?php echo $row['codigo'];?>">
                <button type="button" class="btn btn-warning btn-md">Adicionar Tarefa</button>
            </div>

        </div>

    </div>

</div>