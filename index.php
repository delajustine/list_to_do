<?php header("Content-Type: text/html; charset=ISO-8859-1", true);

require_once('MVC/dao/ProjetoDAO.php');
require_once('MVC/dao/TarefaDAO.php');

try{

    $dao = new ProjetoDAO(null);
    $daoT = new TarefaDAO(null);
    
    $rs = $dao->getLista(0,999);
    $numRows = $rs->rowCount();

}
catch(Exception $e){
}

?>
<!DOCTYPE html>
<html lang="pt">
    <head>

        <?php include("includes/head.php"); ?>
        
    </head>

    <body>

        <header class="header">

            <?php include('includes/header.php') ?>

        </header>

       
        <div id="conteudo">

            <div class="container">

                <div class="row">

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="menu-left">

                        <div class="title-add">
                            <h2>Gerenciamento de projetos</h2>
                        </div><!-- END TITLE-ADD-->

                        <p>Voc� pode adicionar novos projetos no bot�o abaixo.</p>

                        <div class="btn-add">

                            <button type="button" class="btn btn-primary btn-lg" id="btn-add-project">Adicionar Projeto</button>

                        </div><!-- END BTN-ADD-->
                        
                    </div><!-- END COL -->

                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">

                        <div id="last-project-add"></div>
                        
                        <?php 
                            if ($rs != null){
                                while($row = $rs->fetch(PDO::FETCH_ASSOC)) {
                        ?>

                                <div class="panel panel-primary" id="painel-<?php echo $row['codigo'];?>">

                                    <div class="panel-heading">
                                    
                                        <div class="pull-left title-list-project">
                                            <?php echo $row['nome'];?>
                                        </div>

                                        <div class="pull-right btn-delete-project" id="<?php echo $row['codigo'];?>">
                                            
                                        </div>

                                        <div class="clearfix"></div>

                                    </div>

                                    <div class="panel-body">

                                        <div id="list-task-<?php echo $row['codigo'];?>">
                                        
                                            <?php 
                                            $rsTask = $daoT->getListaProjeto($row['codigo']); 
                                            $numRowsTask = $rsTask->rowCount();
                                            ?>

                                            <?php if($numRowsTask > 0){?>
                                                <div class="table-responsive" >
                                                   
                                                    <table class="table table-striped">
                                                        
                                                        <thead>
                                                            <tr>
                                                               
                                                                <th>Tarefa</th>
                                                                <th>Prioridade</th>
                                                                <th>Status</th>
                                                                <th>Entrega</th>
                                                                <th>Tempo Estimado</th>
                                                                <th width="100" colspan="2">A��es</th>
                                                            </tr>
                                                        </thead>
                                                        
                                                        <tbody>

                                                            <?php while($rowTask = $rsTask->fetch(PDO::FETCH_ASSOC)) { ?>
                                                                
                                                                <tr id="linha-<?php echo $rowTask['codigo'];?>">
                                                                    
                                                                   
                                                                    <td><?php echo $rowTask['tarefa'];?></td>

                                                                    <td><?php echo utf8_decode($daoT->getPrioridade($rowTask['prioridade']));?></td>
                                                                    
                                                                    <td><?php echo utf8_decode($daoT->getStatus($rowTask['status']));?></td>
                                                                    
                                                                    <td><?php echo $rowTask['entrega'];?></td>
                                                                    
                                                                    <td><?php echo $rowTask['tempo'];?> hora(s)</td>

                                                                    <td width="50">
                                                                        <button type="button" class="btn btn-primary btn-xs btn-edit-task" id="<?php echo $rowTask['codigo'];?>">Editar</button>
                                                                    </td>
                                                                    <td width="50">
                                                                        <button type="button" class="btn btn-danger btn-xs btn-delete-task" id="<?php echo $rowTask['codigo'];?>">Excluir</button>
                                                                    </td>

                                                                </tr>

                                                            <?php }?>

                                                        </tbody>

                                                        
                                                    </table>

                                                </div><!-- END table-responsive -->

                                            <?php }else{?>

                                                <div class="add-first-task">
                                                    Adicione a primeira tarefa no projeto.
                                                </div>

                                            <?php }?>

                                            

                                            <div class="btn-add-task" id="<?php echo $row['codigo'];?>">
                                                <button type="button" class="btn btn-warning btn-md">Adicionar Tarefa</button>
                                            </div>

                                        </div>

                                    </div>
                                    
                                </div> <!-- END PANEL -->

                        <?php 

                            } // END IF
                                } // END WHILE
                        ?>

                        

                    </div><!-- END COL -->

                </div><!-- END ROW -->

            </div>
            <!-- FIM CONTAINER -->

        </div>
        <!-- FIM CONTEUDO -->

        <?php include("includes/footer.php"); ?>

    </body>

</html>