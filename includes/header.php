<section id="modal-form-edit-task">

	<div class="container">
					
		<div class="row">

			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
			
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

				<div class="icon-close" id="btn-close-task-edit"></div>

				<div class="box-modal">

					<div class="row">

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

							<div class="title-form">

								<h2>Gerenciamento de tarefa</h2>

							</div><!-- END TITLE-FORM -->

						</div><!-- END COL -->

					</div><!-- END ROW -->
					
					<form id="form-task-edit" class="form-horizontal">

						<input type="hidden" name="Etask" id="Etask">

						<div id="form-task-edit"></div>
						
					</form>

					<div class="row" style="position: relative;">

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
							
							<div style="text-align: right; margin-top: 25px;">
								<button type="button" class="btn btn-primary btn-md" onclick="javascript:APP.editTask();">Editar tarefa</button>
							</div>

							<div class="warning-input">
								<p>Preencha todos os campos do formul�rio.</p>
							</div>

							<div class="success-input">
								<p>Cadastro efetuado com sucesso.</p>
							</div>

							<div class="error-input">
								<p>Ocorreu um erro interno, contate o suporte.</p>
							</div>

						</div><!-- END COL -->

					</div><!-- END ROW -->

				</div><!-- END BOX MODAL -->
			
			</div><!-- END COL -->

			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div><!-- END COL -->
		
		</div> <!-- END ROW -->

	</div><!-- END CONTAINER -->

</section>



<section id="modal-form-task">

	<div class="container">
					
		<div class="row">

			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
			
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

				<div class="icon-close" id="btn-close-task"></div>

				<div class="box-modal">

					<div class="row">

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

							<div class="title-form">

								<h2>Cadastro de tarefa</h2>

							</div><!-- END TITLE-FORM -->

						</div><!-- END COL -->

					</div><!-- END ROW -->
					
					<form id="form-task" class="form-horizontal">
						
						<div class="row" style="margin-bottom: 20px;">

							<input type="hidden" name="EcodProject" id="EcodProject">

							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								
								<input type="text" class="form-control required " id="Etarefa" name="Etarefa" placeholder="T�tulo da tarefa">

							</div><!-- END COL -->

							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								
								<select id="Eprioridiade" name="Eprioridiade" class="form-control required">
                                	<option value="">Prioridade</option>
                                	<option value="1">Alta</option>
                                	<option value="2">M�dia</option>
                                	<option value="3">Pequena</option>
                            	</select>

							</div><!-- END COL -->

						</div><!-- END ROW -->

						<div class="row">

							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								
								<select id="Estatus" name="Estatus" class="form-control required">
                                	<option value="">Status</option>
                                	<option value="1">Finalizada</option>
                                	<option value="2">Em desenvolvimento</option>
                                	<option value="3">Em revis�o</option>
                                	<option value="4">Atrasada</option>
                            	</select>

							</div><!-- END COL -->

							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								
								<input type="text" class="form-control required " id="EdataEntrega" name="EdataEntrega" placeholder="Data de entrega">

							</div><!-- END COL -->

							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								
								<input type="text" class="form-control required " id="Etempo" name="Etempo" placeholder="Tempo estimado">

							</div><!-- END COL -->

						</div><!-- END ROW -->

					</form>

					<div class="row" style="position: relative;">

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
							
							<div style="text-align: right; margin-top: 25px;">
								<button type="button" class="btn btn-primary btn-md" id="btn-send-task" onclick="javascript:APP.sendTask();">Cadastrar tarefa</button>
							</div>

							<div class="warning-input">
								<p>Preencha todos os campos do formul�rio.</p>
							</div>

							<div class="success-input">
								<p>Cadastro efetuado com sucesso.</p>
							</div>

							<div class="error-input">
								<p>Ocorreu um erro interno, contate o suporte.</p>
							</div>

						</div><!-- END COL -->

					</div><!-- END ROW -->

				</div><!-- END BOX MODAL -->
			
			</div><!-- END COL -->

			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div><!-- END COL -->
		
		</div> <!-- END ROW -->

	</div><!-- END CONTAINER -->

</section>


<section id="modal-delete-task">

	<div class="container">
					
		<div class="row">

			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
			
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

				<div class="icon-close" id="btn-close-task-delete"></div>

				<div class="box-modal">

					<div class="row">

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

							<div class="title-delete">

								<h2>Tem certeza que deseja deletar essa tarefa ?</h2>

							</div><!-- END TITLE-FORM -->

						</div><!-- END COL -->

					</div><!-- END ROW -->
					
					<input type="hidden" name="EcodDeleteTask" id="EcodDeleteTask">

					<div class="row" style="position: relative;">

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
							
							<div style="text-align: right; margin-top: 25px;">
								<button type="button" class="btn btn-danger btn-md" id="action-delete-task">Deletar tarefa</button>
							</div>

							<div class="success-input">
								<p>Tarefa deletada com sucesso.</p>
							</div>

							<div class="error-input">
								<p>Ocorreu um erro interno, contate o suporte.</p>
							</div>

						</div><!-- END COL -->

					</div><!-- END ROW -->

				</div><!-- END BOX MODAL -->
			
			</div><!-- END COL -->

			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div><!-- END COL -->
		
		</div> <!-- END ROW -->

	</div><!-- END CONTAINER -->

</section>


<section id="modal-form-project">

	<div class="container">
					
		<div class="row">

			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
			
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

				<div class="icon-close" id="btn-close-project"></div>

				<div class="box-modal">

					<div class="row">

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

							<div class="title-form">

								<h2>Cadastro de projeto</h2>

							</div><!-- END TITLE-FORM -->

						</div><!-- END COL -->

					</div><!-- END ROW -->
					
					<form id="form-project" class="form-horizontal">
						
						<div class="row">

							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								
								<input type="text" class="form-control required " id="Enome" name="Enome" placeholder="Nome do projeto">

							</div><!-- END COL -->

						</div><!-- END ROW -->

					</form>

					<div class="row" style="position: relative;">

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
							
							<div style="text-align: right; margin-top: 25px;">
								<button type="button" class="btn btn-primary btn-md" onclick="javascript:APP.sendProject();">Cadastrar projeto</button>
							</div>

							<div class="warning-input">
								<p>Preencha todos os campos do formul�rio.</p>
							</div>

							<div class="success-input">
								<p>Cadastro efetuado com sucesso.</p>
							</div>

							<div class="error-input">
								<p>Ocorreu um erro interno, contate o suporte.</p>
							</div>

						</div><!-- END COL -->

					</div><!-- END ROW -->

				</div><!-- END BOX MODAL -->
			
			</div><!-- END COL -->

			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div><!-- END COL -->
		
		</div> <!-- END ROW -->

	</div><!-- END CONTAINER -->

</section>

<section id="modal-delete-project">

	<div class="container">
					
		<div class="row">

			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
			
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

				<div class="icon-close" id="btn-close-project-delete"></div>

				<div class="box-modal">

					<div class="row">

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

							<div class="title-delete">

								<h2>Tem certeza que deseja deletar esse projeto e todas suas tarefas ?</h2>

							</div><!-- END TITLE-FORM -->

						</div><!-- END COL -->

					</div><!-- END ROW -->
					
					<input type="hidden" name="EcodDeleteProject" id="EcodDeleteProject">

					<div class="row" style="position: relative;">

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
							
							<div style="text-align: right; margin-top: 25px;">
								<button type="button" class="btn btn-danger btn-md" id="action-delete-project">Deletar projeto</button>
							</div>

							<div class="success-input">
								<p>Projeto deletado com sucesso.</p>
							</div>

							<div class="error-input">
								<p>Ocorreu um erro interno, contate o suporte.</p>
							</div>

						</div><!-- END COL -->

					</div><!-- END ROW -->

				</div><!-- END BOX MODAL -->
			
			</div><!-- END COL -->

			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div><!-- END COL -->
		
		</div> <!-- END ROW -->

	</div><!-- END CONTAINER -->

</section>


<section id="bg-top">

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
				<div class="nav-top">
					<h1>Lista de tarefas - To Do</h1>
				</div>

			</div><!-- END COL -->

		</div><!-- END ROW -->
		
	</div> <!-- END CONTAINER -->

</section>