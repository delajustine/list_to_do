<section id="bg-footer">

	<div class="container">

		<div class="row">

			<div class="row">

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					
					<p>Teste realizado por: <span class="name-footer">Mauricio Delajustine</span></p>

				</div><!-- END COL -->

			</div><!-- END ROW -->

		</div><!-- END CONTAINER -->

	</div>

</section>


<!-- <script type="text/javascript" src="<?php //echo base_url('js/jquery/jquery-2.1.1.min.js'); ?>"></script> -->

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script type="text/javascript" src="<?php echo base_url('js/jquery-mask/jquery.mask.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('js/bootstrap/bootstrap.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('js/app.js?v=1'); ?>"></script>

<script type="text/javascript">

	$(document).ready(function() {
		$('#EdataEntrega').mask('00/00/0000', {'translation': {0: {pattern: /[0-9*]/}}});
		$('#Etempo').mask('00:00', {'translation': {0: {pattern: /[0-9*]/}}});
	});

</script>
