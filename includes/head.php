<?php
	define('ENVIRONMENT', 'development');

	if (defined('ENVIRONMENT')) {
		switch (ENVIRONMENT) {
			case 'development':
				error_reporting(E_ALL);
				define('BASEFOLDER', 'list-to-do/');
				break;

			case 'production':
				error_reporting(0);	
				define('BASEFOLDER', '');
				break;

			default:
				exit('O ambiente do aplicativo não está definido corretamente.');
		}
	} else {
		exit('O ambiente do aplicativo não está definido corretamente.');
	}

    function base_url($url = null) {
 
        return $base_url = "http://$_SERVER[HTTP_HOST]/".BASEFOLDER."$url";
    }

?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<meta name="theme-color" content="#00793F">
<meta name="msapplication-navbutton-color" content="#00793F">
<meta name="apple-mobile-web-app-status-bar-style" content="#00793F">

<link href="<?php echo base_url('css/bootstrap/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" media="screen" />

<link href="<?php echo base_url('css/reset.css?v=1'); ?>" rel="stylesheet" type="text/css" media="screen" />

<link href="<?php echo base_url('css/fonts.css?v=1'); ?>" rel="stylesheet" type="text/css" media="screen" />

<link href="<?php echo base_url('css/main.css?v=1'); ?>" rel="stylesheet" type="text/css" media="screen" />
