var root_path = window.location.protocol + '//' + document.location.hostname + '/list-to-do';
//var root_path = window.location.protocol + '//' + document.location.hostname;

var APP = {

	init: function () {
		this.setup();
		this.start();
		this.bind();
	},

	setup: function() {
		this.showModalProject = $('#btn-add-project');
		this.hideModalProject = $('#btn-close-project');
		this.showModalProjectDelete = $('.btn-delete-project');
		this.hideModalProjectDelete = $('#btn-close-project-delete');
		this.codProjectDelete = $('#btn-delete-project div');
		this.actionDeleteProject = $('#action-delete-project');

		this.showModalTask = $('.btn-add-task');
		this.hideModalTask = $('#btn-close-task');
		this.showModalTaskDelete = $('.btn-delete-task');
		this.hideModalTaskDelete = $('#btn-close-task-delete');
		this.actionDeleteTask = $('#action-delete-task');
		this.showModalTaskEdit = $('.btn-edit-task');
		this.hideModalTaskEdit = $('#btn-close-task-edit');	
	},

	start: function() {

		console.log('APP started.');

	},

	/* PADRÕES */

	showModal: function(modal){
		$('#'+modal).show();
	},

	hideModal: function(modal){
		$('#'+modal).hide();
	},

	showWarningInput: function(){

		$(".warning-input").fadeIn(1000); 
		setTimeout(function() {
			$(".warning-input").fadeOut(1000)
		}, 1500);

	},

	showSuccessInput: function(modal){

		$(".success-input").fadeIn(1000); 
		setTimeout(function() {
			$(".success-input").fadeOut(1000);
			$('#'+modal).fadeOut(1000);
		}, 1500);

	},

	showErrorInput: function(){

		$(".error-input").fadeIn(1000); 
		setTimeout(function() {
			$(".error-input").fadeOut(1000)
		}, 1500);

	},

	trim: function(str) {
	    var str2 = str.replace(/^\s\s*/, ''),
	                  ws = /\s/,
			  i = str2.length;
	    while (ws.test(str2.charAt(--i)));
	    return str2.slice(0, i + 1);
	},

	/* FUNÇÃO PARA VALIDAR OS CAMPOS DOS FORMULÁRIOS */

	validaForm: function(form){
		
		var required = $("#"+form + "  .required");
		var req = 0;

		for(i=0;i<required.length;i++){

			if($(required[i]).val() == ""){
				
				var classe = $(required[i]).attr('class');
				if(classe.search("required") != -1){	
					$(required[i]).addClass('erroInput');
				}
				req = 1;

			}else{
				
				var classe2 = $(required[i]).attr('class');
				
				if(classe2.search("erroInput") != -1){
					$(required[i]).removeClass('erroInput');
				}

			}
		}
		
		if(req == 1){

			this.showWarningInput();
			return false;

		}else if(req == 0){

			return true;

		}
	},

	/* END FUNÇÃO PARA VALIDAR OS CAMPOS DOS FORMULÁRIOS */

	/* END PADRÕES */

	deleteProject: function(){

		var cod = $('#EcodDeleteProject').val();
		var dados = 'cod='+cod;

		jQuery.ajax({

			type: "POST",
			url: root_path+"/MVC/action/ExcluirProjeto.php",
			data: dados,

			success: function(msg){

				var msg = $.trim(msg);

				if (msg == "ok"){
					$('#painel-'+cod).fadeOut(500);
					APP.showSuccessInput('modal-delete-project');
				}
				
				if (msg == 'erro'){	
					APP.showErrorInput();
				}
				
			}
		});

	},

	sendProject: function(){

		if(APP.validaForm("form-project")){
			var dados = $("#form-project").serialize();

			jQuery.ajax({

				type: "POST",
				url: root_path+"/MVC/action/IncluirProjeto.php",
				data: dados,

				success: function(msg){

					var msg2 = $.trim(msg);

					if (msg2 == "ok"){
						$("#form-project")[0].reset();
						APP.showSuccessInput('modal-form-project');	
						$("#last-project-add").load('last-project.php',function(){
							APP.init();
						});
						
					}
					
					if (msg2 == 'erro'){	
						APP.showErrorInput();
					}
					
				}
			});
		}

	},

	editTask: function(){

		if(APP.validaForm("form-task-edit")){
			var dados = $("#form-task-edit").serialize();

			jQuery.ajax({

				type: "POST",
				url: root_path+"/MVC/action/AlterarTarefa.php",
				data: dados,

				success: function(msg){

					var msg2 = $.trim(msg);

					if (msg2 == "ok"){
						var cod = $('#EcodProject').val();
						APP.showSuccessInput('modal-form-edit-task');	

						$("#list-task-"+cod).load('list-task.php?cod='+cod, function(){
							APP.init();
						});	
					}
					
					if (msg2 == 'erro'){	
						APP.showErrorInput();
					}
					
				}
			});
		}

	},

	sendTask: function(){

		if(APP.validaForm("form-task")){
			var dados = $("#form-task").serialize();

			jQuery.ajax({

				type: "POST",
				url: root_path+"/MVC/action/IncluirTarefa.php",
				data: dados,

				success: function(msg){

					var msg2 = $.trim(msg);

					if (msg2 == "ok"){
						var cod = $('#EcodProject').val();
						$("#form-task")[0].reset();
						APP.showSuccessInput('modal-form-task');	

						$("#list-task-"+cod).load('list-task.php?cod='+cod, function(){
							APP.init();
						});	
					}
					
					if (msg2 == 'erro'){	
						APP.showErrorInput();
					}
					
				}
			});
		}

	},

	checkTask:function(value){
		
		var valor = $('#Echeck-'+value).val();
		if($('#Echeck-'+value).is(':checked')){
		 	console.log('marcado');
		}else{
			console.log('desmarcado');
		}
	},

	deleteTask: function(){

		var cod = $('#EcodDeleteTask').val();
		var dados = 'cod='+cod;

		jQuery.ajax({

			type: "POST",
			url: root_path+"/MVC/action/ExcluirTarefa.php",
			data: dados,

			success: function(msg){

				var msg = $.trim(msg);

				if (msg == "ok"){
					$('#linha-'+cod).fadeOut(500);
					APP.showSuccessInput('modal-delete-task');
				}
				
				if (msg == 'erro'){	
					APP.showErrorInput();
				}
				
			}
		});

	},

	
	bind: function() {

		var _this = this;

		/* PROJECT */

		this.showModalProject.on('click', function() {
			_this.showModal('modal-form-project');
		});

		this.hideModalProject.on('click', function() {
			_this.hideModal('modal-form-project');
		});

		this.showModalProjectDelete.on('click', function() {
			var cod = $(this).attr('id');
			$('#EcodDeleteProject').val(cod);
			_this.showModal('modal-delete-project');
		});

		this.hideModalProjectDelete.on('click', function() {
			$('#EcodDeleteProject').val('');
			_this.hideModal('modal-delete-project');
		});

		this.actionDeleteProject.on('click', function() {
			_this.deleteProject();
		});

		/* END PROJECT */

		/* TASK */

		this.showModalTask.on('click', function() {
			var cod = $(this).attr('id');
			$('#EcodProject').val(cod);
			_this.showModal('modal-form-task');
		});

		this.hideModalTask.on('click', function() {
			$('#EcodProject').val('');
			_this.hideModal('modal-form-task');
		});

		this.showModalTaskDelete.on('click', function() {
			var cod = $(this).attr('id');
			$('#EcodDeleteTask').val(cod);
			_this.showModal('modal-delete-task');
		});

		this.hideModalTaskDelete.on('click', function() {
			$('#EcodDeleteTask').val('');
			_this.hideModal('modal-delete-task');
		});

		this.actionDeleteTask.on('click', function() {
			_this.deleteTask();
		});

		this.showModalTaskEdit.on('click', function() {
			
			var cod = $(this).attr('id');
			
			$('#EcodProjectEdit').val(cod);

			$("#form-task-edit").load('edit-task.php?cod='+cod, function(){
				APP.init();
			});
			
			_this.showModal('modal-form-edit-task');

		});

		this.hideModalTaskEdit.on('click', function() {
			console.log('a');
			$('#EcodDeleteTask').val('');
			_this.hideModal('modal-form-edit-task');
		});


		/* END TASK */
	

	}

};

$(document).ready(function() {
	APP.init();
});
