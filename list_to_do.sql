-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 29-Out-2018 às 16:51
-- Versão do servidor: 5.7.23
-- versão do PHP: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `list_to_do`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `projeto`
--

DROP TABLE IF EXISTS `projeto`;
CREATE TABLE IF NOT EXISTS `projeto` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`codigo`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `projeto`
--

INSERT INTO `projeto` (`codigo`, `nome`, `data`) VALUES
(1, 'Lista de tarefas - To Do', '2018-10-27 01:33:20');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tarefa`
--

DROP TABLE IF EXISTS `tarefa`;
CREATE TABLE IF NOT EXISTS `tarefa` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `projeto` int(10) NOT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tarefa` varchar(250) NOT NULL,
  `prioridade` int(2) NOT NULL,
  `status` int(2) NOT NULL,
  `entrega` varchar(20) NOT NULL,
  `tempo` varchar(20) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=MyISAM AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tarefa`
--

INSERT INTO `tarefa` (`codigo`, `projeto`, `data_cadastro`, `tarefa`, `prioridade`, `status`, `entrega`, `tempo`) VALUES
(1, 1, '2018-10-27 17:01:40', 'Construir estrutura HTML', 2, 2, '24/10/2018', '03:00'),
(74, 1, '2018-10-28 02:25:44', 'Banco de dados - estruturar as tabelas', 3, 1, '18/12/2028', '02:30'),
(4, 1, '2018-10-27 17:28:20', 'Trabalhar no padrão de CSS', 2, 2, '29/10/2018', '05:00'),
(10, 1, '2018-10-27 17:52:27', 'Criar as modais de cadastros', 1, 1, '29/10/2018', '05:00'),
(76, 1, '2018-10-28 03:03:56', 'teste finalizada', 3, 1, '12/09/2018', '02:00'),
(78, 1, '2018-10-28 03:19:14', 'tarefa completa', 1, 1, '20/10/2018', '03:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
