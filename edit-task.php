<?php header("Content-Type: text/html; charset=ISO-8859-1", true);

$codigo = $_GET['cod']; 

require_once('MVC/dao/TarefaDAO.php');

try{

    $daoTask = new TarefaDAO(null);
    $obj = $daoTask->getById($codigo);

}
catch(Exception $e){
}

?>

<div class="row" style="margin-bottom: 20px;">

    <input type="hidden" name="EcodProject" id="EcodProject" value="<?php echo $obj->projeto;?>">

    <input type="hidden" name="Ecodigo" id="Ecodigo" value="<?php echo $obj->codigo;?>">

    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        
        <input type="text" class="form-control required " id="Etarefa" name="Etarefa" placeholder="T�tulo da tarefa" value="<?php echo $obj->tarefa;?>">

    </div><!-- END COL -->

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        
        <select id="Eprioridiade" name="Eprioridiade" class="form-control required">
            <option value="">Prioridade</option>
            <option value="1" <?php if($obj->prioridade == '1'){?>selected="selected"<?php } ?>>Alta</option>
            <option value="2" <?php if($obj->prioridade == '2'){?>selected="selected"<?php } ?>>M�dia</option>
            <option value="3" <?php if($obj->prioridade == '3'){?>selected="selected"<?php } ?>>Pequena</option>
        </select>

    </div><!-- END COL -->

</div><!-- END ROW -->

<div class="row">

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        
        <select id="Estatus" name="Estatus" class="form-control required">
            <option value="">Status</option>
            <option value="1" <?php if($obj->status == '1'){?>selected="selected"<?php } ?>>Finalizada</option>
            <option value="2" <?php if($obj->status == '2'){?>selected="selected"<?php } ?>>Em desenvolvimento</option>
            <option value="3" <?php if($obj->status == '3'){?>selected="selected"<?php } ?>>Em revis�o</option>
            <option value="4" <?php if($obj->status == '4'){?>selected="selected"<?php } ?>>Atrasada</option>
        </select>

    </div><!-- END COL -->

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        
        <input type="text" class="form-control required " id="EdataEntrega" name="EdataEntrega" placeholder="Data de entrega" value="<?php echo $obj->entrega;?>">

    </div><!-- END COL -->

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        
        <input type="text" class="form-control required " id="Etempo" name="Etempo" placeholder="Tempo estimado" value="<?php echo $obj->tempo;?>">

    </div><!-- END COL -->

</div><!-- END ROW -->